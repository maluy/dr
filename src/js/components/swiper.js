function initSwiper() {
  const swiperAboutSmallg = new Swiper('.js-cases', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
      nextEl: '.cases-button-next',
      prevEl: '.cases-button-prev',
    },
    breakpoints: {
      1024: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
    },
  });

  const swiperServicesSmallg = new Swiper('.js-services', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,

    navigation: {
      nextEl: '.services-button-next',
      prevEl: '.services-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
    },
  });

  const swiperProfessionalsSmallg = new Swiper('.js-professionals', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,

    navigation: {
      nextEl: '.professionals-button-next',
      prevEl: '.professionals-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 30,
      },
    },
  });

  const swiperGallerySmallg = new Swiper('.js-gallery', {
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.gallery-button-next',
      prevEl: '.gallery-button-prev',
    },

    breakpoints: {
      570: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
        loop: false,
        grid: {
          rows: 2,
        },
      },
      1200: {
        slidesPerView: 4,
        loop: false,
        grid: {
          rows: 2,
        },
      },
    },
  });

  const swiperPhotoSmallg = new Swiper('.js-photo', {
    slidesPerView: 1,
    loop: true,
    // navigation: {
    //   nextEl: '.photo-button-next',
    //   prevEl: '.photo-button-prev',
    // },

    breakpoints: {
      570: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
        loop: false,
        grid: {
          rows: 4,
        },
      },
      1200: {
        slidesPerView: 4,
        loop: false,
        grid: {
          rows: 4,
        },
      },
    },
  });
}

export default initSwiper;

/* eslint-disable no-trailing-spaces */
// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js and will not be minified.
// Feel free with using ES6 here.

import initAccordion from './components/accordion';
import burgerMenu from './components/burger-menu';
import initSwiper from './components/swiper';
import popups from './components/popup';

(($) => {
  // When DOM is ready
  $(() => {
    //const accordions = new Accordion();
    initSwiper();
    initAccordion();
    burgerMenu.init();
    popups.init();
  });
})(jQuery);

var scrolled;
window.onscroll = function () {
  scrolled = window.pageYOffset || document.documentElement.scrollTop;
  if (scrolled > 100) {
    $('.menu').css({ background: '#393949' });
  }
  if (100 > scrolled) {
    $('.menu').css({ background: 'transparent' });
  }
};

Fancybox.bind('[data-fancybox="gallery"]', {
  animated: false,
  showClass: false,
  hideClass: false,

  dragToClose: false,

  closeButton: 'top',

  Thumbs: false,
  Toolbar: false,

  Carousel: {
    // Enable dots
    Dots: true,

    // Disable touch guestures
    Panzoom: {
      touch: false,
    },

    // Disable sliding animation
    friction: 0,
  },

  Image: {
    zoom: false,
    click: false,
    wheel: false,
    fit: 'contain-w',
  },

  on: {
    init: (fancybox) => (fancybox.prevScrollTop = 0),
    done: (fancybox, slide) => (slide.$el.scrollTop = fancybox.prevScrollTop),
    'Carousel.change': (fancybox, carousel, to, from) => {
      fancybox.prevScrollTop = carousel.slides[from].$el.scrollTop || 0;

      if (carousel.slides[to].$el) {
        carousel.slides[to].$el.scrollTop = fancybox.prevScrollTop;
      }
    },
  },
});

window.addEventListener('DOMContentLoaded', function () {
  [].forEach.call(document.querySelectorAll('.tel'), function (input) {
    var keyCode;
    function mask(event) {
      event.keyCode && (keyCode = event.keyCode);
      var pos = this.selectionStart;
      if (pos < 3) event.preventDefault();
      var matrix = '+7 (___) ___+__+__',
        i = 0,
        def = matrix.replace(/\D/g, ''),
        val = this.value.replace(/\D/g, ''),
        new_value = matrix.replace(/[_\d]/g, function (a) {
          return i < val.length ? val.charAt(i++) || def.charAt(i) : a;
        });
      i = new_value.indexOf('_');
      if (i != -1) {
        i < 5 && (i = 3);
        new_value = new_value.slice(0, i);
      }
      var reg = matrix
        .substr(0, this.value.length)
        .replace(/_+/g, function (a) {
          return '\\d{1,' + a.length + '}';
        })
        .replace(/[+()]/g, '\\$&');
      reg = new RegExp('^' + reg + '$');
      if (!reg.test(this.value) || this.value.length < 5 || (keyCode > 47 && keyCode < 58)) this.value = new_value;
      if (event.type == 'blur' && this.value.length < 5) this.value = '';
    }

    input.addEventListener('input', mask, false);
    input.addEventListener('focus', mask, false);
    input.addEventListener('blur', mask, false);
    input.addEventListener('keydown', mask, false);
  });
});

$(function () {
  $('.js-modal-open').each(function () {
    $(this).on('click', function () {
      var msrc = $(this).data('src');
      $.when($('.js-modal').find('video').attr('src', msrc), $('.js-modal').fadeIn()).done(function () {
        $('.js-modal').find('video').get(0).play();
      });
      return false;
    });
  });
  $('.js-modal-close').on('click', function () {
    $.when($('.js-modal').fadeOut()).done(function () {
      $('.js-modal').find('video').attr('src', '');
    });
    return false;
  });
});
1;

